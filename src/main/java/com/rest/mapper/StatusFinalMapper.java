/**
 * 
 */
package com.rest.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.rest.bean.StatusFinal;

/**
 * @author Dimas Rifqi
 *
 */
public class StatusFinalMapper {
	
	public RowMapper<StatusFinal> getAll = new RowMapper<StatusFinal>() {
		@Override
		public StatusFinal mapRow(ResultSet rs, int rowNum) throws SQLException {
			StatusFinal sf = new StatusFinal();
			sf.setId(rs.getInt("ID"));
			sf.setStatus(rs.getString("STATUS"));
			
			return sf;
		}
	};
	
	public RowMapper<StatusFinal> getById = new RowMapper<StatusFinal>() {
		@Override
		public StatusFinal mapRow(ResultSet rs, int rowNum) throws SQLException {
			StatusFinal sf = new StatusFinal();
			sf.setId(rs.getInt("ID"));
			sf.setStatus(rs.getString("STATUS"));
			
			return sf;
		}
	};

}
