/**
 * 
 */
package com.rest.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.springframework.jdbc.core.RowMapper;

import com.rest.bean.Request;

/**
 * @author Dimas Rifqi
 *
 */
public class RequestMapper {
	
	public RowMapper<Request> generalMapper = new RowMapper<Request>() {
		@Override
		public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
			Request request = new Request();
			request.setId(rs.getInt("ID"));
			request.setAppsId(rs.getString("APPS_ID"));
			request.setServer(rs.getString("SERVER"));
			
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			String finalDate = df.format(rs.getTimestamp("REQUEST_DATE"));
			request.setRequestDateFormated(finalDate);
			
			request.setRequestBy(rs.getString("REQUEST_BY"));
			request.setReceiveBy(rs.getString("RECEIVE_BY"));
			request.setCc(rs.getString("CC"));
			request.setContent(rs.getString("CONTENT"));
			request.setStatusFinal(rs.getInt("STATUS_FINAL_ID"));
			request.setTypeId(rs.getInt("TYPE_ID"));
			request.setTypeRequest(rs.getString("TYPE_REQUEST_ID"));
			request.setSendEmailResponse(rs.getString("SEND_EMAIL_RESPON"));
			request.setSendEmailResult(rs.getString("SEND_EMAIL_RESULT"));
			
			return request;
		}
	};
	
	public RowMapper<Request> getAll = new RowMapper<Request>() {
		@Override
		public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
			Request request = new Request();
			request.setId(rs.getInt("ID"));
			request.setAppsId(rs.getString("APPS_ID"));
			request.setServer(rs.getString("SERVER"));
			
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			String finalDate = df.format(rs.getTimestamp("REQUEST_DATE"));
			request.setRequestDateFormated(finalDate);
			
			request.setRequestBy(rs.getString("REQUEST_BY"));
			request.setReceiveBy(rs.getString("RECEIVE_BY"));
			request.setCc(rs.getString("CC"));
			request.setContent(rs.getString("CONTENT"));
			request.setStatusFinal(rs.getInt("STATUS_FINAL_ID"));
			request.setTypeId(rs.getInt("TYPE_ID"));
			request.setTypeRequest(rs.getString("TYPE_REQUEST_ID"));
			
			return request;
		}
	};
	
	public RowMapper<Request> getById = new RowMapper<Request>() {
		@Override
		public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
			Request request = new Request();
			request.setId(rs.getInt("ID"));
			request.setAppsId(rs.getString("APPS_ID"));
			request.setServer(rs.getString("SERVER"));
			
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			String finalDate = df.format(rs.getTimestamp("REQUEST_DATE"));
			request.setRequestDateFormated(finalDate);
			
			request.setRequestBy(rs.getString("REQUEST_BY"));
			request.setReceiveBy(rs.getString("RECEIVE_BY"));
			request.setCc(rs.getString("CC"));
			request.setContent(rs.getString("CONTENT"));
			request.setStatusFinal(rs.getInt("STATUS_FINAL_ID"));
			request.setTypeId(rs.getInt("TYPE_ID"));
			request.setTypeRequest(rs.getString("TYPE_REQUEST_ID"));
			
			return request;
		}
	};
	
	public RowMapper<Request> getByAppsId = new RowMapper<Request>() {
		@Override
		public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
			Request request = new Request();
			
			request.setId(rs.getInt("ID"));
			request.setAppsId(rs.getString("APPS_ID"));
			request.setServer(rs.getString("SERVER"));
			
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			String finalDate = df.format(rs.getTimestamp("REQUEST_DATE"));
			request.setRequestDateFormated(finalDate);
			
			request.setRequestBy(rs.getString("REQUEST_BY"));
			request.setReceiveBy(rs.getString("RECEIVE_BY"));
			request.setCc(rs.getString("CC"));
			request.setContent(rs.getString("CONTENT"));
			request.setStatusFinal(rs.getInt("STATUS_FINAL_ID"));
			request.setTypeId(rs.getInt("TYPE_ID"));
			request.setTypeRequest(rs.getString("TYPE_REQUEST_ID"));
			
			return request;
		}
	};
	
	public RowMapper<Request> getByJenkinsJobAndStatusFinal = new RowMapper<Request>() {
		@Override
		public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
			Request request = new Request();
			
			request.setId(rs.getInt("ID"));
			request.setAppsId(rs.getString("APPS_ID"));
			request.setJenkinsJobId(rs.getString("JENKINS_JOB_ID"));
			request.setServer(rs.getString("SERVER"));
			
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			String finalDate = df.format(rs.getTimestamp("REQUEST_DATE"));
			request.setRequestDateFormated(finalDate);
			
			request.setRequestBy(rs.getString("REQUEST_BY"));
			request.setReceiveBy(rs.getString("RECEIVE_BY"));
			request.setCc(rs.getString("CC"));
			request.setContent(rs.getString("CONTENT"));
			request.setStatusFinal(rs.getInt("STATUS_FINAL_ID"));
			request.setTypeId(rs.getInt("TYPE_ID"));
			request.setTypeRequest(rs.getString("TYPE_REQUEST_ID"));
			
			return request;
		}
	};
	
	public RowMapper<Request> getByDate = new RowMapper<Request>() {
		@Override
		public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
			Request request = new Request();
			
			request.setId(rs.getInt("ID"));
			request.setAppsId(rs.getString("APPS_ID"));
			request.setJenkinsJobId(rs.getString("JENKINS_JOB_ID"));
			request.setServer(rs.getString("SERVER"));
			
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			String finalDate = df.format(rs.getTimestamp("REQUEST_DATE"));
			request.setRequestDateFormated(finalDate);
			
			request.setRequestBy(rs.getString("REQUEST_BY"));
			request.setReceiveBy(rs.getString("RECEIVE_BY"));
			request.setCc(rs.getString("CC"));
			request.setContent(rs.getString("CONTENT"));
			request.setStatusFinal(rs.getInt("STATUS_FINAL_ID"));
			request.setTypeId(rs.getInt("TYPE_ID"));
			request.setTypeRequest(rs.getString("TYPE_REQUEST_ID"));
			
			return request;
		}
	};
	
	public RowMapper<Request> getByName = new RowMapper<Request>() {
		@Override
		public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
			Request request = new Request();
			request.setId(rs.getInt("ID"));
			request.setAppsId(rs.getString("APPS_ID"));
			request.setServer(rs.getString("SERVER"));
			
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			String finalDate = df.format(rs.getTimestamp("REQUEST_DATE"));
			request.setRequestDateFormated(finalDate);
			
			request.setRequestBy(rs.getString("REQUEST_BY"));
			request.setReceiveBy(rs.getString("RECEIVE_BY"));
			request.setCc(rs.getString("CC"));
			request.setContent(rs.getString("CONTENT"));
			request.setStatusFinal(rs.getInt("STATUS_FINAL_ID"));
			request.setTypeId(rs.getInt("TYPE_ID"));
			request.setTypeRequest(rs.getString("TYPE_REQUEST_ID"));
			
			return request;
		}
	};
}
