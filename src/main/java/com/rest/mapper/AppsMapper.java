package com.rest.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.rest.bean.Apps;

public class AppsMapper {
	
	public RowMapper<Apps> getAll = new RowMapper<Apps>() {

		@Override
		public Apps mapRow(ResultSet rs, int i) throws SQLException {
			
			Apps apps = new Apps();
			apps.setId(rs.getString("ID"));
			apps.setName(rs.getString("NAME"));
			apps.setCategoryId(rs.getInt("CATEGORY_ID"));
			apps.setTypeId(rs.getInt("TYPE_ID"));
			
			return apps;
		}
		
	};
	
	public RowMapper<Apps> getById = new RowMapper<Apps>() {

		@Override
		public Apps mapRow(ResultSet rs, int i) throws SQLException {
			
			Apps apps = new Apps();
			apps.setId(rs.getString("ID"));
			apps.setName(rs.getString("NAME"));
			apps.setCategoryId(rs.getInt("CATEGORY_ID"));
			apps.setTypeId(rs.getInt("TYPE_ID"));
			
			return apps;
		}
		
	};
	
	public RowMapper<Apps> getByName = new RowMapper<Apps>() {

		@Override
		public Apps mapRow(ResultSet rs, int i) throws SQLException {
			
			Apps apps = new Apps();
			apps.setId(rs.getString("ID"));
			apps.setName(rs.getString("NAME"));
			apps.setCategoryId(rs.getInt("CATEGORY_ID"));
			apps.setTypeId(rs.getInt("TYPE_ID"));
			
			return apps;
		}
		
	};
	
	public RowMapper<List<Apps>> getByType = new RowMapper<List<Apps>>() {

		@SuppressWarnings("unchecked")
		@Override
		public List<Apps> mapRow(ResultSet rs, int i) throws SQLException {
			
			Apps apps = new Apps();
			apps.setId(rs.getString("ID"));
			apps.setName(rs.getString("NAME"));
			apps.setCategoryId(rs.getInt("CATEGORY_ID"));
			apps.setTypeId(rs.getInt("TYPE_ID"));
			
			return (List<Apps>) apps;
		}
		
	};

}
