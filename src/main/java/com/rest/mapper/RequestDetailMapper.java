/**
 * 
 */
package com.rest.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rest.bean.RequestDetail;

/**
 * @author Dimas Rifqi
 *
 */
public class RequestDetailMapper {
	
	public RowMapper<RequestDetail> getAll = new RowMapper<RequestDetail>() {

		@Override
		public RequestDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
			RequestDetail rd = new RequestDetail();
			rd.setId(rs.getInt("ID"));
			rd.setRequestId(rs.getInt("REQUEST_ID"));
			rd.setBuildNo(rs.getInt("BUILD_NO"));
			rd.setStatus(rs.getString("STATUS"));
			rd.setStatusFinalId(rs.getInt("STATUS_FINAL_ID"));
			rd.setJenkinsJobId(rs.getString("JENKINS_JOB_ID"));
			
			return rd;
		}
	};
	
	public RowMapper<RequestDetail> getById = new RowMapper<RequestDetail>() {

		@Override
		public RequestDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
			RequestDetail rd = new RequestDetail();
			rd.setId(rs.getInt("ID"));
			rd.setRequestId(rs.getInt("REQUEST_ID"));
			rd.setBuildNo(rs.getInt("BUILD_NO"));
			rd.setStatus(rs.getString("STATUS"));
			rd.setStatusFinalId(rs.getInt("STATUS_FINAL_ID"));
			rd.setJenkinsJobId(rs.getString("JENKINS_JOB_ID"));
			
			return rd;
		}
	};
	
	public RowMapper<RequestDetail> getByJenkinsJobId = new RowMapper<RequestDetail>() {

		@Override
		public RequestDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
			RequestDetail rd = new RequestDetail();
			rd.setId(rs.getInt("ID"));
			rd.setRequestId(rs.getInt("REQUEST_ID"));
			rd.setBuildNo(rs.getInt("BUILD_NO"));
			rd.setStatus(rs.getString("STATUS"));
			rd.setStatusFinalId(rs.getInt("STATUS_FINAL_ID"));
			rd.setJenkinsJobId(rs.getString("JENKINS_JOB_ID"));
			
			return rd;
		}
	};
	
	public RowMapper<RequestDetail> getByRequestId = new RowMapper<RequestDetail>() {

		@Override
		public RequestDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
			RequestDetail rd = new RequestDetail();
			rd.setId(rs.getInt("ID"));
			rd.setRequestId(rs.getInt("REQUEST_ID"));
			rd.setBuildNo(rs.getInt("BUILD_NO"));
			rd.setStatus(rs.getString("STATUS"));
			rd.setStatusFinalId(rs.getInt("STATUS_FINAL_ID"));
			rd.setJenkinsJobId(rs.getString("JENKINS_JOB_ID"));
			
			return rd;
		}
	};
	
	public RowMapper<RequestDetail> getByBuildNo = new RowMapper<RequestDetail>() {

		@Override
		public RequestDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
			RequestDetail rd = new RequestDetail();
			rd.setId(rs.getInt("ID"));
			rd.setRequestId(rs.getInt("REQUEST_ID"));
			rd.setBuildNo(rs.getInt("BUILD_NO"));
			rd.setStatus(rs.getString("STATUS"));
			rd.setStatusFinalId(rs.getInt("STATUS_FINAL_ID"));
			rd.setJenkinsJobId(rs.getString("JENKINS_JOB_ID"));
			
			return rd;
		}
	};
	
	public RowMapper<RequestDetail> getByStatus = new RowMapper<RequestDetail>() {

		@Override
		public RequestDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
			RequestDetail rd = new RequestDetail();
			rd.setId(rs.getInt("ID"));
			rd.setRequestId(rs.getInt("REQUEST_ID"));
			rd.setBuildNo(rs.getInt("BUILD_NO"));
			rd.setStatus(rs.getString("STATUS"));
			rd.setStatusFinalId(rs.getInt("STATUS_FINAL_ID"));
			rd.setJenkinsJobId(rs.getString("JENKINS_JOB_ID"));
			
			return rd;
		}
	};
	
}
