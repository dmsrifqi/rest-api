package com.rest.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rest.bean.Type;

public class TypeMapper {
	public RowMapper<Type> getAll = new RowMapper<Type>() {

		@Override
		public Type mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Type type = new Type();
			type.setId(rs.getInt("id"));
			type.setName(rs.getString("name"));
				
			return type;
		}
		
	};
	
	public RowMapper<Type> getId = new RowMapper<Type>() {

		@Override
		public Type mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Type type = new Type();
			type.setId(rs.getInt("id"));
			type.setName(rs.getString("name"));
				
			return type;
		}
		
	};
	
	public RowMapper<Type> getName = new RowMapper<Type>() {

		@Override
		public Type mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Type type = new Type();
			type.setId(rs.getInt("id"));
			type.setName(rs.getString("name"));
				
			return type;
		}
		
	};
}
