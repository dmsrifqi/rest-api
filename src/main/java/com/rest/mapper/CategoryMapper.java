package com.rest.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rest.bean.Category;

public class CategoryMapper {
	public RowMapper<Category> getAll = new RowMapper<Category>() {

		@Override
		public Category mapRow(ResultSet rs, int i) throws SQLException {
			
			Category category = new Category();
			category.setId(rs.getInt("ID"));
			category.setName(rs.getString("NAME"));
			
			return category;
		}
		
	};
	
	public RowMapper<Category> getById = new RowMapper<Category>() {

		@Override
		public Category mapRow(ResultSet rs, int i) throws SQLException {
			
			Category category = new Category();
			category.setId(rs.getInt(1));
			category.setName(rs.getString("NAME"));
			
			return category;
		}
		
	};
	
	public RowMapper<Category> getByName = new RowMapper<Category>() {

		@Override
		public Category mapRow(ResultSet rs, int i) throws SQLException {
			
			Category category = new Category();
			category.setId(rs.getInt(1));
			category.setName(rs.getString("NAME"));
			
			return category;
		}
		
	};
}
