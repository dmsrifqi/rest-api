package com.rest.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rest.bean.JenkinsJob;

public class JenkinsJobMapper {
	
	public RowMapper<JenkinsJob> getAll = new RowMapper<JenkinsJob>() {
		@Override
		public JenkinsJob mapRow(ResultSet rs, int i) throws SQLException {
			JenkinsJob job = new JenkinsJob();
			job.setId(rs.getString("ID"));
			job.setName(rs.getString("NAME"));
			job.setDesc(rs.getString("_DESC"));
			job.setAppsId(rs.getString("APPS_ID"));
			job.setTypeRequest(rs.getString("TYPE_REQUEST_ID"));
			job.setServer(rs.getString("SERVER"));
			
			return job;
		}
	};
	
	public RowMapper<JenkinsJob> getById = new RowMapper<JenkinsJob>() {
		@Override
		public JenkinsJob mapRow(ResultSet rs, int i) throws SQLException {
			JenkinsJob job = new JenkinsJob();
			job.setId(rs.getString("ID"));
			job.setName(rs.getString("NAME"));
			job.setDesc(rs.getString("_DESC"));
			job.setAppsId(rs.getString("APPS_ID"));
			job.setTypeRequest(rs.getString("TYPE_REQUEST_ID"));
			job.setServer(rs.getString("SERVER"));
			
			return job;
		}
	};
	
	public RowMapper<JenkinsJob> getByName = new RowMapper<JenkinsJob>() {
		@Override
		public JenkinsJob mapRow(ResultSet rs, int i) throws SQLException {
			JenkinsJob job = new JenkinsJob();
			job.setId(rs.getString("ID"));
			job.setName(rs.getString("NAME"));
			job.setDesc(rs.getString("_DESC"));
			job.setAppsId(rs.getString("APPS_ID"));
			job.setTypeRequest(rs.getString("TYPE_REQUEST_ID"));
			job.setServer(rs.getString("SERVER"));
			
			return job;
		}
	};
}
