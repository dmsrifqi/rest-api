package com.rest.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rest.bean.TypeRequest;

public class TypeRequestMapper {
	public RowMapper<TypeRequest> getAll = new RowMapper<TypeRequest>() {

		@Override
		public TypeRequest mapRow(ResultSet rs, int i) throws SQLException {
			
			TypeRequest TypeRequest = new TypeRequest();
			TypeRequest.setId(rs.getString("ID"));
			TypeRequest.setName(rs.getString("NAME"));
			
			return TypeRequest;
		}
		
	};
	
	public RowMapper<TypeRequest> getById = new RowMapper<TypeRequest>() {

		@Override
		public TypeRequest mapRow(ResultSet rs, int i) throws SQLException {
			
			TypeRequest TypeRequest = new TypeRequest();
			TypeRequest.setId(rs.getString(1));
			TypeRequest.setName(rs.getString("NAME"));
			
			return TypeRequest;
		}
		
	};
	
	public RowMapper<TypeRequest> getByName = new RowMapper<TypeRequest>() {

		@Override
		public TypeRequest mapRow(ResultSet rs, int i) throws SQLException {
			
			TypeRequest TypeRequest = new TypeRequest();
			TypeRequest.setId(rs.getString(1));
			TypeRequest.setName(rs.getString("NAME"));
			
			return TypeRequest;
		}
		
	};
}
