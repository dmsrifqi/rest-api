/**
 * 
 */
package com.rest.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rest.bean.Sysinfo;

/**
 * @author Dimas Rifqi
 *
 */
public class SysinfoMapper {
	
	public RowMapper<Sysinfo> getAll = new RowMapper<Sysinfo>() {

		@Override
		public Sysinfo mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Sysinfo sys = new Sysinfo();
			sys.setId(rs.getInt("ID"));
			sys.setHost(rs.getString("HOST"));
			sys.setPort(rs.getString("PORT"));
			sys.setUsername(rs.getString("USERNAME"));
			sys.setPassword(rs.getString("PASSWORD"));
			sys.setParam(rs.getString("PARAM"));
			
			return sys;
		}
	};
	
	public RowMapper<Sysinfo> getById = new RowMapper<Sysinfo>() {

		@Override
		public Sysinfo mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Sysinfo sys = new Sysinfo();
			sys.setId(rs.getInt("ID"));
			sys.setHost(rs.getString("HOST"));
			sys.setPort(rs.getString("PORT"));
			sys.setUsername(rs.getString("USERNAME"));
			sys.setPassword(rs.getString("PASSWORD"));
			sys.setParam(rs.getString("PARAM"));
			
			return sys;
		}
	};

}
