package com.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.bean.ApiResponse;
import com.rest.bean.JenkinsJob;
import com.rest.dao.JenkinsJobDao;

@Controller
@RestController
@RequestMapping("/api/jenkins-job")
public class JenkinsJobController {
	
	@Autowired
	private JenkinsJobDao jenkinsJobDao;
	
	@GetMapping("/getAll")
	public ResponseEntity<ApiResponse<JenkinsJob>> getAll() {
		
		ApiResponse<JenkinsJob> response = new ApiResponse<JenkinsJob>();
		
		try {
			List<JenkinsJob> result = jenkinsJobDao.getAll();
			if(!result.isEmpty()) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getById", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<JenkinsJob>> getById(@RequestBody JenkinsJob job) {
		
		ApiResponse<JenkinsJob> response = new ApiResponse<JenkinsJob>();
		try {
			
			JenkinsJob result = jenkinsJobDao.getById(job);
			if(result != null) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path="/getByName", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<JenkinsJob>> getByName(@RequestBody JenkinsJob job) {
		
		ApiResponse<JenkinsJob> response = new ApiResponse<JenkinsJob>();
		try {
			
			JenkinsJob result = jenkinsJobDao.getByName(job);
			if(result != null) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path="/insert", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<JenkinsJob>> insert(@RequestBody JenkinsJob job) {
		
		ApiResponse<JenkinsJob> response = new ApiResponse<>();
		
		try {
			Boolean result = jenkinsJobDao.insert(job);
			if( result ) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Insert data successfully !");
				
				return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.OK);
			} else {
				response.setStatus("400");
				response.setMessage("Failed");
				response.setDisplay_message("Insert data failure !");
				
				return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("502");
			response.setMessage("Failed");
			response.setDisplay_message("Internal server error");
			
			return new ResponseEntity<ApiResponse<JenkinsJob>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
}
