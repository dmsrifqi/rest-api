package com.rest.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.bean.ApiResponse;

@Controller
@RestController
@RequestMapping("/api")
public class ConnectionController {
	
	@Value("${spring.datasource.url}")
	private String host;
	
	@Value("${spring.datasource.username}")
	private String username;
	
	@Value("${spring.datasource.password}")
	private String password;
	
	@GetMapping("/connection")
	public ResponseEntity<ApiResponse<String>> connection() throws IOException{
		
		ApiResponse<String> response = new ApiResponse<String>();
		
		try {
			
			Connection conn = DriverManager.getConnection(host, username, password);
			
			if(!conn.isClosed()) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Koneksi nyambung !");
				response.insertNewData(conn.getCatalog());
				
				return new ResponseEntity<ApiResponse<String>>(response, HttpStatus.OK);
			} else {
				
				response.setStatus("500");
				response.setMessage("Failed");
				response.setDisplay_message("Koneksi gagal !");
				
				return new ResponseEntity<ApiResponse<String>>(response, HttpStatus.BAD_GATEWAY);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failure");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<String>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
