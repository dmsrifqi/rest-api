package com.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.bean.ApiResponse;
import com.rest.bean.Apps;
import com.rest.dao.AppsDao;

@Controller
@RestController
@RequestMapping("/api/apps")
public class AppsController {
	
	@Autowired
	private AppsDao appsDao;
	
	@GetMapping("/getAll")
	public ResponseEntity<ApiResponse<Apps>> getAll() {
		
		ApiResponse<Apps> response = new ApiResponse<Apps>();
		
		try {
			List<Apps> result = appsDao.getAll();
			if(!result.isEmpty()) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getById", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Apps>> getById(@RequestBody Apps app) {
		
		ApiResponse<Apps> response = new ApiResponse<Apps>();
		try {
			
			Apps result = appsDao.getById(app);
			if(result != null) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getByName", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Apps>> getByName(@RequestBody Apps app) {
		
		ApiResponse<Apps> response = new ApiResponse<Apps>();
		
		try {
			
			Apps result = appsDao.getByName(app);
			if(result != null) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.OK);
			} else {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				
				return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/insert", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Apps>> insert(@RequestBody Apps app) {
		
		ApiResponse<Apps> response = new ApiResponse<>();
		
		try {
			Boolean result = appsDao.insert(app);
			if( result ) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Insert data successfully !");
				
				return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.OK);
			} else {
				response.setStatus("400");
				response.setMessage("Failed");
				response.setDisplay_message("Insert data failure !");
				
				return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("502");
			response.setMessage("Failed");
			response.setDisplay_message("Internal server error");
			
			return new ResponseEntity<ApiResponse<Apps>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
}
