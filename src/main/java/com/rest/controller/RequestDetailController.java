package com.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.bean.ApiResponse;
import com.rest.bean.RequestDetail;
import com.rest.dao.RequestDetailDao;

@Controller
@RestController
@RequestMapping("/api/request-detail")
public class RequestDetailController {
	
	@Autowired
	private RequestDetailDao requestDetailDao;
	
	@GetMapping("/getAll")
	public ResponseEntity<ApiResponse<RequestDetail>> getAll() {
		
		ApiResponse<RequestDetail> response = new ApiResponse<RequestDetail>();
		
		try {
			List<RequestDetail> result = requestDetailDao.getAll();
			if(!result.isEmpty()) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getById", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<RequestDetail>> getById(@RequestBody RequestDetail rd) {
		
		ApiResponse<RequestDetail> response = new ApiResponse<RequestDetail>();
		
		try {
			
			RequestDetail result = requestDetailDao.getById(rd);
			if(!result.equals(null)) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getByJenkinsJobFinish", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<RequestDetail>> getByJenkinsJobFinish(@RequestBody RequestDetail rd) {
		
		ApiResponse<RequestDetail> response = new ApiResponse<RequestDetail>();
		
		try {
			
			RequestDetail result = requestDetailDao.getByJenkinsJobFinish(rd);
			if(result != null) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getByJenkinsJob", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<RequestDetail>> getByJenkinsJob(@RequestBody RequestDetail rd) {
		
		ApiResponse<RequestDetail> response = new ApiResponse<RequestDetail>();
		
		try {
			
			RequestDetail result = requestDetailDao.getByJenkinsJob(rd);
			if(result != null) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getByReqIdAndBuildNo", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<RequestDetail>> getByBuildNo(@RequestBody RequestDetail rd) {
		
		ApiResponse<RequestDetail> response = new ApiResponse<RequestDetail>();
		
		try {
			
			RequestDetail result = requestDetailDao.getByReqIdAndBuildNo(rd);
			if(result != null) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/updateReqDetailByJenJobAndReqId", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<RequestDetail>> updateReqDetailByJenJobAndReqId(@RequestBody RequestDetail rd) {
		
		ApiResponse<RequestDetail> response = new ApiResponse<>();
		
		try {
			RequestDetail result = requestDetailDao.updateReqDetailByJenJobAndReqId(rd);
			if( result != null ) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Update data successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			} else {
				response.setStatus("400");
				response.setMessage("Failed");
				response.setDisplay_message("Update data failure !");
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("502");
			response.setMessage("Failed");
			response.setDisplay_message("Internal server error");
			
			return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path="/insert", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<RequestDetail>> insert(@RequestBody RequestDetail rd) {
		
		ApiResponse<RequestDetail> response = new ApiResponse<>();
		
		try {
			Boolean result = requestDetailDao.insert(rd);
			if( result ) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Insert data successfully !");
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.OK);
			} else {
				response.setStatus("400");
				response.setMessage("Failed");
				response.setDisplay_message("Insert data failure !");
				
				return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("502");
			response.setMessage("Failed");
			response.setDisplay_message("Internal server error");
			
			return new ResponseEntity<ApiResponse<RequestDetail>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
}
