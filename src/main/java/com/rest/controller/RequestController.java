package com.rest.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.bean.ApiResponse;
import com.rest.bean.Request;
import com.rest.dao.RequestDao;

@Controller
@RestController
@RequestMapping("/api/request")
public class RequestController {
	
	@Autowired
	private RequestDao requestDao;
	
	@GetMapping("/getAll")
	public ResponseEntity<ApiResponse<Request>> getAll() {
		
		ApiResponse<Request> response = new ApiResponse<Request>();
		
		try {
			List<Request> result = requestDao.getAll();
			if(!result.isEmpty()) {
				
//				DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//				for(Request r: result) {
//					String strDate = df.format(r.getRequestDate());
//					r.setRequestDateFormated(strDate);
//				}
				
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path="/getById", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Request>> getById(@RequestBody Request request) {
		
		ApiResponse<Request> response = new ApiResponse<Request>();
		
		try {
			Request result = requestDao.getById(request);
			if(!result.equals(null)) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path="/getByJenkinsJobAndStatusFinal", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Request>> getByJenkinsJobAndStatusFinal(@RequestBody Request request) {
		
		ApiResponse<Request> response = new ApiResponse<Request>();
		
		try {
			List<Request> result = requestDao.getByJenkinsJobAndStatusFinal(request);
			if(result !=null) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path="/updateSendEmailResponse", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Request>> updateSendEmailResponse(@RequestBody Request request) {
		
		ApiResponse<Request> response = new ApiResponse<Request>();
		
		try {
			Boolean result = requestDao.updateSendEmailResponse(request);
			if(result) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path="/updateSendEmailResult", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Request>> updateSendEmailResult(@RequestBody Request request) {
		
		ApiResponse<Request> response = new ApiResponse<Request>();
		
		try {
			Boolean result = requestDao.updateSendEmailResult(request);
			if(result) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Update successfully !");
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path="/updateReqByStatusFinal", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Request>> updateStatusFinal(@RequestBody Request request) {
		
		ApiResponse<Request> response = new ApiResponse<Request>();
		
		try {
			Boolean result = requestDao.updateReqByStatusFinal(request);
			if(result) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Update successfully !");
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Update failed !");
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path="/insert", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Request>> insert(@RequestBody Request request) {
		
		ApiResponse<Request> response = new ApiResponse<>();
		
		try {
			Request result = requestDao.insert(request);
			if( result !=null ) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Insert data successfully !");
				response.insertNewData(result);
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.OK);
			} else {
				response.setStatus("400");
				response.setMessage("Failed");
				response.setDisplay_message("Insert data failure !");
				
				return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("502");
			response.setMessage("Failed");
			response.setDisplay_message("Internal server error");
			
			return new ResponseEntity<ApiResponse<Request>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
