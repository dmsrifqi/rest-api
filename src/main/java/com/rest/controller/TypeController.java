package com.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.bean.ApiResponse;
import com.rest.bean.Type;
import com.rest.dao.TypeDao;

/**
 * @author Dimas Rifqi
 *
 */

@Controller
@RestController
@RequestMapping("/api/type")
public class TypeController {
	
	@Autowired
	private TypeDao typeDao;

	@GetMapping("/getAll")
	public ResponseEntity<ApiResponse<Type>> getAll() {
		
		ApiResponse<Type> response = new ApiResponse<Type>();
		
		try {
			List<Type> result = typeDao.getAll();
			if(!result.isEmpty()) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getById", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Type>> getById(@RequestBody Type type) {
		
		ApiResponse<Type> response = new ApiResponse<Type>();
		
		try {
			Type result = typeDao.getById(type);
			if(!result.equals(null)) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getByName", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Type>> getByName(@RequestBody Type type) {
		
		ApiResponse<Type> response = new ApiResponse<Type>();
		
		try {
			Type result = typeDao.getByName(type);
			if(!result.equals(null)) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/insert", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<Type>> insert(@RequestBody Type type) {
		
		ApiResponse<Type> response = new ApiResponse<>();
		
		try {
			Boolean result = typeDao.insert(type);
			if( result ) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Insert data successfully !");
				
				return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.OK);
			} else {
				response.setStatus("400");
				response.setMessage("Failed");
				response.setDisplay_message("Insert data failure !");
				
				return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("502");
			response.setMessage("Failed");
			response.setDisplay_message("Internal server error");
			
			return new ResponseEntity<ApiResponse<Type>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
}
