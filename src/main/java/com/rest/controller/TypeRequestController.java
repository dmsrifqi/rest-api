package com.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.bean.ApiResponse;
import com.rest.bean.TypeRequest;
import com.rest.dao.TypeRequestDao;

/**
 * @author Dimas Rifqi
 *
 */

@Controller
@RestController
@RequestMapping("/api/type-request")
public class TypeRequestController {
	
	@Autowired
	private TypeRequestDao typeRequestDao;

	@GetMapping("/getAll")
	public ResponseEntity<ApiResponse<TypeRequest>> getAll() {
		
		ApiResponse<TypeRequest> response = new ApiResponse<TypeRequest>();
		
		try {
			List<TypeRequest> result = typeRequestDao.getAll();
			if(!result.isEmpty()) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.setData(result);
				
				return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getById", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<TypeRequest>> getById(@RequestBody TypeRequest type) {
		
		ApiResponse<TypeRequest> response = new ApiResponse<TypeRequest>();
		
		try {
			TypeRequest result = typeRequestDao.getById(type);
			if(!result.equals(null)) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/getByName", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<TypeRequest>> getByName(@RequestBody TypeRequest type) {
		
		ApiResponse<TypeRequest> response = new ApiResponse<TypeRequest>();
		
		try {
			TypeRequest result = typeRequestDao.getByName(type);
			if(!result.equals(null)) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Query successfully !");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.OK);
			} else {
				response.setStatus("204");
				response.setMessage("Success");
				response.setDisplay_message("Data null");
				response.addData(result);
				
				return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("500");
			response.setMessage("Failed");
			response.setDisplay_message(e.getMessage());
			
			return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path="/insert", consumes="application/json", produces="application/json")
	public ResponseEntity<ApiResponse<TypeRequest>> insert(@RequestBody TypeRequest type) {
		
		ApiResponse<TypeRequest> response = new ApiResponse<>();
		
		try {
			Boolean result = typeRequestDao.insert(type);
			if( result ) {
				response.setStatus("200");
				response.setMessage("Success");
				response.setDisplay_message("Insert data successfully !");
				
				return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.OK);
			} else {
				response.setStatus("400");
				response.setMessage("Failed");
				response.setDisplay_message("Insert data failure !");
				
				return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("502");
			response.setMessage("Failed");
			response.setDisplay_message("Internal server error");
			
			return new ResponseEntity<ApiResponse<TypeRequest>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
}
