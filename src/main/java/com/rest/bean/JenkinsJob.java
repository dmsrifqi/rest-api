package com.rest.bean;

/**
 * @author Dimas Rifqi
 *
 */

public class JenkinsJob {
	private String id;
	private String name;
	private String desc;
	private String appsId;
	private String typeRequest;
	private String server;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getAppsId() {
		return appsId;
	}
	public void setAppsId(String appsId) {
		this.appsId = appsId;
	}
	public String getTypeRequest() {
		return typeRequest;
	}
	public void setTypeRequest(String typeRequest) {
		this.typeRequest = typeRequest;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
}
