/**
 * 
 */
package com.rest.bean;

/**
 * @author Dimas Rifqi
 *
 */

public class RequestDetail {
	public int id;
	public int requestId;
	public int buildNo;
	public String status;
	public int statusFinalId;
	public String jenkinsJobId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRequestId() {
		return requestId;
	}
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
	public int getBuildNo() {
		return buildNo;
	}
	public void setBuildNo(int buildNo) {
		this.buildNo = buildNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getStatusFinalId() {
		return statusFinalId;
	}
	public void setStatusFinalId(int statusFinalId) {
		this.statusFinalId = statusFinalId;
	}
	public String getJenkinsJobId() {
		return jenkinsJobId;
	}
	public void setJenkinsJobId(String jenkinsJobId) {
		this.jenkinsJobId = jenkinsJobId;
	}
}
