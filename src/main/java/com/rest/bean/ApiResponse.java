package com.rest.bean;

import java.util.ArrayList;
import java.util.List;

public class ApiResponse<T> {
	
	private String status;
	private String message;
	private String display_message;
	private List<T> data;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDisplay_message() {
		return display_message;
	}

	public void setDisplay_message(String display_message) {
		this.display_message = display_message;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public void insertNewData(T data) {
		this.data = new ArrayList<T>();
		this.data.add(data);
	}

	public void addData(T data) {
		if (this.data == null) {
			this.data = new ArrayList<T>();
		}
		this.data.add(data);
	}

}
