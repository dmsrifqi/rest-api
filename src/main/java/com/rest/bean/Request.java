package com.rest.bean;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;

public class Request {
	private int id;
	private String jenkinsJobId;
	private String appsId;
	private int typeId;
	private String typeRequest;
	private String server;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy/MM/dd HH:mm:ss")
	private Timestamp requestDate;
	private String requestDateFormated;
	private String requestBy;
	private String receiveBy;
	private String cc;
	private String content;
	private int statusFinal;
	private String sendEmailResponse;
	private String sendEmailResult;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getJenkinsJobId() {
		return jenkinsJobId;
	}
	public void setJenkinsJobId(String jenkinsJobId) {
		this.jenkinsJobId = jenkinsJobId;
	}
	public String getAppsId() {
		return appsId;
	}
	public void setAppsId(String appsId) {
		this.appsId = appsId;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getTypeRequest() {
		return typeRequest;
	}
	public void setTypeRequest(String typeRequest) {
		this.typeRequest = typeRequest;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public Timestamp getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Timestamp requestDate) {
		this.requestDate = requestDate;
	}
	public String getRequestDateFormated() {
		return requestDateFormated;
	}
	public void setRequestDateFormated(String requestDateFormated) {
		this.requestDateFormated = requestDateFormated;
	}
	public String getRequestBy() {
		return requestBy;
	}
	public void setRequestBy(String requestBy) {
		this.requestBy = requestBy;
	}
	public String getReceiveBy() {
		return receiveBy;
	}
	public void setReceiveBy(String receiveBy) {
		this.receiveBy = receiveBy;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getStatusFinal() {
		return statusFinal;
	}
	public void setStatusFinal(int statusFinal) {
		this.statusFinal = statusFinal;
	}
	public String getSendEmailResponse() {
		return sendEmailResponse;
	}
	public void setSendEmailResponse(String sendEmailResponse) {
		this.sendEmailResponse = sendEmailResponse;
	}
	public String getSendEmailResult() {
		return sendEmailResult;
	}
	public void setSendEmailResult(String sendEmailResult) {
		this.sendEmailResult = sendEmailResult;
	}
}