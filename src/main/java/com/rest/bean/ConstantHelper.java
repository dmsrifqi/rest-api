/**
 * 
 */
package com.rest.bean;

/**
 * @author Dimas Rifqi
 *
 */
public class ConstantHelper {
	
	public String APPS_NAME=null;
	public String TYPE=null;
	
	public String appsName(String s){
		if(s.equalsIgnoreCase("SP")) {
			APPS_NAME = "Spotlight";
		} else if(s.equalsIgnoreCase("QN")) {
			APPS_NAME = "Qnock";
		} else if(s.equalsIgnoreCase("HS")) {
			APPS_NAME = "Hearsay";
		} else if(s.equalsIgnoreCase("SQ")) {
			APPS_NAME = "Squad";
		} else if(s.equalsIgnoreCase("KB")) {
			APPS_NAME  ="Kawanbaik";
		}
		return APPS_NAME;
	}
	
	public String type(String s){
		if(s.equalsIgnoreCase("1")) {
			TYPE = "CMS";
		} else if(s.equalsIgnoreCase("2")) {
			TYPE = "WEB";
		} else {
			TYPE = "API";
		}
		return TYPE;
	}
}
