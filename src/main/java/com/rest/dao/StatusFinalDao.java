/**
 * 
 */
package com.rest.dao;

import java.util.List;
import com.rest.bean.StatusFinal;

/**
 * @author Dimas Rifqi
 *
 */
public interface StatusFinalDao {
	public List<StatusFinal> getAll();
	public StatusFinal getById(StatusFinal type);
	public Boolean insert(StatusFinal type);
}
