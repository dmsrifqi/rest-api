/**
 * 
 */
package com.rest.dao;

import java.util.List;
import com.rest.bean.Request;

/**
 * @author Dimas Rifqi
 *
 */
public interface RequestDao {
	public List<Request> getAll();
	public Request getById(Request r);
	public Request getByAppsId(Request r);
	public List<Request> getByJenkinsJobAndStatusFinal(Request r);
	public Request getByDate(Request r);
	public Boolean updateSendEmailResponse(Request r);
	public Boolean updateSendEmailResult(Request r);
	public Boolean updateReqByStatusFinal(Request r);
	public Request insert(Request r);
}
