package com.rest.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rest.bean.RequestDetail;
import com.rest.dao.RequestDetailDao;
import com.rest.mapper.RequestDetailMapper;

@Transactional
@Repository
public class RequestDetailDaoImpl implements RequestDetailDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<RequestDetail> getAll() {
		String sql = "SELECT * FROM REQUEST_DETAIL";
		List<RequestDetail> result = jdbcTemplate.query(sql, new RequestDetailMapper().getAll);
		
		return result;
	}

	@Override
	public RequestDetail getById(RequestDetail rd) {
		String sql = "SELECT * FROM REQUEST_DETAIL WHERE ID=?";
		List<RequestDetail> result = jdbcTemplate.query(sql, new Object[] {rd.getId()},new RequestDetailMapper().getById);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	//MAILREADER:272
	@Override
	public RequestDetail getByJenkinsJobFinish(RequestDetail rd) {
		String sql = "SELECT * FROM REQUEST_DETAIL WHERE JENKINS_JOB_ID=? AND REQUEST_ID=? AND STATUS_FINAL_ID=1";
		List<RequestDetail> result = jdbcTemplate.query(sql, new Object[] {rd.getJenkinsJobId(), rd.getRequestId()},new RequestDetailMapper().getByJenkinsJobId);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	//MAILREADER:273
	@Override
	public RequestDetail getByJenkinsJob(RequestDetail rd) {
		String sql = "SELECT * FROM REQUEST_DETAIL WHERE JENKINS_JOB_ID=?";
		List<RequestDetail> result = jdbcTemplate.query(sql, new Object[] {rd.getJenkinsJobId()},new RequestDetailMapper().getByJenkinsJobId);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	//ENGGA DIPAKE
	@Override
	public RequestDetail getByReqIdAndBuildNo(RequestDetail rd) {
		String sql = "SELECT * FROM REQUEST_DETAIL INNER JOIN REQUEST ON REQUEST_DETAIL.REQUEST_ID=REQUEST.ID "
				+ "WHERE REQUEST_DETAIL.REQUEST_ID=? AND REQUEST_DETAIL.BUILD_NO=?";
		List<RequestDetail> result = jdbcTemplate.query(sql, new Object[] {rd.getRequestId(), rd.getBuildNo()},new RequestDetailMapper().getByBuildNo);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public RequestDetail getByReqId(RequestDetail rd) {
		String sql = "SELECT * FROM REQUEST_DETAIL WHERE REQUEST_ID=?";
		List<RequestDetail> result = jdbcTemplate.query(sql, new Object[] {rd.getRequestId()},new RequestDetailMapper().getById);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}

	@Override
	public Boolean insert(RequestDetail rd) {
		String sql = "INSERT INTO REQUEST_DETAIL(request_id, build_no, status, status_final_id, jenkins_job_id) VALUES (?,?,?,?,?)";
		try {
			jdbcTemplate.update(sql, new Object[] {
				rd.getRequestId(),
				rd.getBuildNo(),
				rd.getStatus(),
				rd.getStatusFinalId(),
				rd.getJenkinsJobId()
			});
			
			return true;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public RequestDetail updateReqDetailByJenJobAndReqId(RequestDetail rd) {
		String sql = "UPDATE REQUEST_DETAIL SET status=?, status_final_id=? WHERE jenkins_job_id=? AND request_id=? AND status_final_id=999";
		try {
			jdbcTemplate.update(sql, new Object[] {
				rd.getStatus(),
				rd.getStatusFinalId(),
				rd.getJenkinsJobId(),
				rd.getRequestId()
			});
			
			RequestDetail result = getByReqId(rd);
			
			return result;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}
}
