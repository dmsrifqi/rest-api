package com.rest.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.rest.bean.Category;
import com.rest.dao.CategoryDao;
import com.rest.mapper.CategoryMapper;

@Repository
@Transactional
public class CategoryDaoImpl implements CategoryDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Category> getAll() {
		
		String sql = "SELECT * FROM CATEGORY";
		List<Category> result = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Category>(Category.class));

		return result;
	}

	@Override
	public Category getById(Category category) {
		
		String sql = "SELECT * FROM CATEGORY WHERE ID=?";
		List<Category> result = jdbcTemplate.query(sql, new Object[] {category.getId()}, new CategoryMapper().getById);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
		
	}

	@Override
	public Category getByName(Category category) {
		
		String sql = "SELECT * FROM CATEGORY WHERE NAME=?";
		List<Category> result = jdbcTemplate.query(sql, new Object[] {category.getName()}, new CategoryMapper().getByName);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}

	@Override
	public Boolean insert(Category category) {
		
		try {
			String sql = "INSERT INTO CATEGORY(name) VALUES (?)";
			jdbcTemplate.update(sql, new Object[] {
					category.getName()
			});
			
			return true;
		} catch (DataAccessException e) {
			e.printStackTrace();
			
			return false;
		}
	}
	
	
}
