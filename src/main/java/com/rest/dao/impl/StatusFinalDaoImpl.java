/**
 * 
 */
package com.rest.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rest.bean.StatusFinal;
import com.rest.dao.StatusFinalDao;
import com.rest.mapper.StatusFinalMapper;

/**
 * @author Dimas Rifqi
 *
 */
@Repository
@Transactional
public class StatusFinalDaoImpl implements StatusFinalDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<StatusFinal> getAll() {
		String sql = "SELECT * FROM STATUS_FINAL";
		List<StatusFinal> result = jdbcTemplate.query(sql, new BeanPropertyRowMapper<StatusFinal>(StatusFinal.class));
		
		return result;
	}

	@Override
	public StatusFinal getById(StatusFinal sf) {
		String sql="SELECT * FROM STATUS_FINAL WHERE ID=?";
		List<StatusFinal> result = jdbcTemplate.query(sql, new Object[] {sf.getId()}, new StatusFinalMapper().getById);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}

	@Override
	public Boolean insert(StatusFinal sf) {
		try {
			String sql = "INSERT INTO STATUS_FINAL (id, status) VALUES (?,?)";
			jdbcTemplate.update(sql, new Object[] {
					sf.getId(),
					sf.getStatus()
			});
			return true;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return false;
		}
	}
}
