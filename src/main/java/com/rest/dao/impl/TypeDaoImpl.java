package com.rest.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rest.bean.Type;
import com.rest.dao.TypeDao;
import com.rest.mapper.TypeMapper;

@Repository
@Transactional
public class TypeDaoImpl implements TypeDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Type> getAll() {
		
		String sql = "SELECT * FROM TYPE";
		List<Type> result = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Type>(Type.class));
		
		return result;
	}

	@Override
	public Type getById(Type type) {
		String sql = "SELECT * FROM TYPE WHERE ID=?";
		List<Type> result = jdbcTemplate.query(sql, new Object[] {type.getId()}, new TypeMapper().getId);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}

	@Override
	public Type getByName(Type type) {
		
		String sql = "SELECT * FROM TYPE WHERE NAME=?";
		List<Type> result = jdbcTemplate.query(sql, new Object[] {type.getName()}, new TypeMapper().getName);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public Boolean insert(Type type) {
		
		String sql = "INSERT INTO TYPE(name) VALUES (?)";
		
		try {
			jdbcTemplate.update(sql, new Object[] {
				type.getName()
			});
			
			return true;
		} catch (DataAccessException e) {
			e.printStackTrace();
			
			return false;
		}
		
	}
}
