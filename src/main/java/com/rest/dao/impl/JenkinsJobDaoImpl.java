/**
 * 
 */
package com.rest.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rest.bean.JenkinsJob;
import com.rest.dao.JenkinsJobDao;
import com.rest.mapper.JenkinsJobMapper;

/**
 * @author Dimas Rifqi
 *
 */
@Transactional
@Repository
public class JenkinsJobDaoImpl implements JenkinsJobDao {
	
	@Autowired
	public JdbcTemplate jdbcTemplate;

	@Override
	public List<JenkinsJob> getAll() {
		String sql = "SELECT * FROM JENKINS_JOB";
		List<JenkinsJob> result = jdbcTemplate.query(sql, new BeanPropertyRowMapper<JenkinsJob>(JenkinsJob.class));
		
		return result;
	}

	@Override
	public JenkinsJob getById(JenkinsJob job) {
		String sql = "SELECT * FROM JENKINS_JOB WHERE ID=?";
		List<JenkinsJob> result = jdbcTemplate.query(sql, new Object[] {job.getId()}, new JenkinsJobMapper().getById);
		if(result.size() > 0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public JenkinsJob getByName(JenkinsJob job) {
		String sql = "SELECT * FROM JENKINS_JOB WHERE NAME=?";
		List<JenkinsJob> result = jdbcTemplate.query(sql, new Object[] {job.getName()}, new JenkinsJobMapper().getByName);
		if(result.size() > 0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	

	@Override
	public Boolean insert(JenkinsJob job) {
		String sql = "INSERT INTO JENKINS_JOB(id, name, _desc, apps_id, type_request_id, server) VALUES (?,?,?,?,?,?)";
		try {
			jdbcTemplate.update(sql, new Object[] {
				job.getId(),
				job.getName(),
				job.getDesc(),
				job.getTypeRequest(),
				job.getServer()
			});
			
			return true;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return false;
		}
		
	}
}
