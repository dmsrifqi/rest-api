/**
 * 
 */
package com.rest.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rest.bean.Sysinfo;
import com.rest.dao.SysinfoDao;
import com.rest.mapper.SysinfoMapper;

/**
 * @author Dimas Rifqi
 *
 */
@Repository
@Transactional
public class SysinfoDaoImpl implements SysinfoDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Sysinfo> getAll() {
		
		String sql = "SELECT * FROM SYS_INFO";
		List<Sysinfo> result =  jdbcTemplate.query(sql, new BeanPropertyRowMapper<Sysinfo>(Sysinfo.class));
		
		return result;
	}

	@Override
	public Sysinfo getById(Sysinfo sys) {
		
		String sql = "SELECT * FROM SYS_INFO WHERE ID=?";
		List<Sysinfo> result = jdbcTemplate.query(sql, new Object[] {sys.getId()}, new SysinfoMapper().getById);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}

	@Override
	public Boolean insert(Sysinfo sys) {
		
		String sql = "INSERT INTO SYS_INFO(subject, host, port, username, password, param)"
				+ "VALUES(?,?,?,?,?,?)";
		try {
			jdbcTemplate.update(sql, new Object[] {
					sys.getSubject(),
					sys.getHost(),
					sys.getPort(),
					sys.getUsername(),
					sys.getPassword(),
					sys.getParam()
			});
			
			return true;
			
		} catch (DataAccessException e) {
			e.printStackTrace();
			
			return false;
		}
	}

}
