package com.rest.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rest.bean.TypeRequest;
import com.rest.dao.TypeRequestDao;
import com.rest.mapper.TypeRequestMapper;

@Repository
@Transactional
public class TypeRequestDaoImpl implements TypeRequestDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<TypeRequest> getAll() {
		
		String sql = "SELECT * FROM TYPE_REQUEST";
		List<TypeRequest> result = jdbcTemplate.query(sql, new BeanPropertyRowMapper<TypeRequest>(TypeRequest.class));
		
		return result;
	}

	@Override
	public TypeRequest getById(TypeRequest typeRequest) {
		String sql = "SELECT * FROM TYPE_REQUEST WHERE ID=?";
		List<TypeRequest> result = jdbcTemplate.query(sql, new Object[] {typeRequest.getId()}, new TypeRequestMapper().getById);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}

	@Override
	public TypeRequest getByName(TypeRequest typeRequest) {
		
		String sql = "SELECT * FROM TYPE_REQUEST WHERE NAME=?";
		List<TypeRequest> result = jdbcTemplate.query(sql, new Object[] {typeRequest.getName()}, new TypeRequestMapper().getByName);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public Boolean insert(TypeRequest typeRequest) {
		
		String sql = "INSERT INTO TYPE_REQUEST(name) VALUES (?)";
		
		try {
			jdbcTemplate.update(sql, new Object[] {
				typeRequest.getName()
			});
			
			return true;
		} catch (DataAccessException e) {
			e.printStackTrace();
			
			return false;
		}
		
	}
}
