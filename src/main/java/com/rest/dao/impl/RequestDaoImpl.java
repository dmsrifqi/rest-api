package com.rest.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rest.bean.Request;
import com.rest.dao.RequestDao;
import com.rest.mapper.RequestMapper;

@Transactional
@Repository
public class RequestDaoImpl implements RequestDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Request> getAll() {
		String sql = "select id, apps_id, server, request_date, "
				+ "request_by, receive_by, cc, content, status_final_id, type_id, type_request_id from request";
		
		List<Request> result = jdbcTemplate.query(sql, new RequestMapper().getAll);
		
		return result;
	}

	@Override
	public Request getById(Request request) {
		String sql = "SELECT * FROM REQUEST WHERE ID=?";
		List<Request> result = jdbcTemplate.query(sql, new Object[] {request.getId()},new RequestMapper().getById);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public Request getByAppsId(Request request) {
		String sql = "SELECT * FROM REQUEST WHERE APPS_ID=?";
		List<Request> result = jdbcTemplate.query(sql, new Object[] {request.getAppsId()},new RequestMapper().getByAppsId);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public List<Request> getByJenkinsJobAndStatusFinal(Request request) {
		String sql = "SELECT * FROM REQUEST WHERE JENKINS_JOB_ID=? AND STATUS_FINAL_ID=999";
		List<Request> result = jdbcTemplate.query(sql, new Object[] {request.getJenkinsJobId()},new RequestMapper().getByJenkinsJobAndStatusFinal);
		
		return result;
	}
	
	@Override
	public Request getByDate(Request request) {
		String sql = "select  * from request where request_date=?";
		List<Request> result = jdbcTemplate.query(sql, new Object[] {request.getRequestDateFormated()},new RequestMapper().getByDate);
		if(result.size()>0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public Boolean updateSendEmailResponse(Request request) {
		String sql = "UPDATE REQUEST SET SEND_EMAIL_RESPON='SENT' WHERE ID=?";
		try {
			jdbcTemplate.update(sql, new Object[] {request.getId()});
			return true;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Boolean updateSendEmailResult(Request request) {
		String sql = "UPDATE REQUEST SET SEND_EMAIL_RESULT='SENT' WHERE ID=?";
		try {
			jdbcTemplate.update(sql, new Object[] {request.getId()});
			return true;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public Boolean updateReqByStatusFinal(Request request) {
		String sql = "UPDATE REQUEST SET status_final_id=? WHERE jenkins_job_id=? AND id=? AND status_final_id=999";
		try {
			jdbcTemplate.update(sql, new Object[] {
				request.getStatusFinal(),
				request.getJenkinsJobId(),
				request.getId()
			});
			
			return true;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public Request insert(Request request) {
		String sql = "INSERT INTO REQUEST(apps_id, jenkins_job_id, server, request_date, request_by, receive_by, "
					+ "cc, content, status_final_id, type_id, type_request_id)"
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		try {
			jdbcTemplate.update(sql, new Object[] {
				request.getAppsId(),
				request.getJenkinsJobId(),
				request.getServer(),
				request.getRequestDateFormated(),
				request.getRequestBy(),
				request.getReceiveBy(),
				request.getCc(),
				request.getContent(),
				request.getStatusFinal(),
				request.getTypeId(),
				request.getTypeRequest()
			});
			
			return getByDate(request);
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}
}
