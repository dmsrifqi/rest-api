/**
 * 
 */
package com.rest.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rest.bean.Apps;
import com.rest.dao.AppsDao;
import com.rest.mapper.AppsMapper;

/**
 * @author Dimas Rifqi
 *
 */
@Transactional
@Repository
public class AppsDaoImpl implements AppsDao {
	
	@Autowired
	public JdbcTemplate jdbcTemplate;

	@Override
	public List<Apps> getAll() {
		
		String sql = "SELECT * FROM APPS";
		List<Apps> result = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Apps>(Apps.class));
		
		return result;
	}

	@Override
	public Apps getById(Apps app) {
		
		String sql = "SELECT * FROM APPS WHERE ID=?";
		List<Apps> result = jdbcTemplate.query(sql, new Object[] {app.getId()}, new AppsMapper().getById);
		if(result.size() > 0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	

	@Override
	public Apps getByName(Apps app) {
		
		String sql = "SELECT * FROM APPS WHERE NAME=?";
		List<Apps> result = jdbcTemplate.query(sql, new Object[] {app.getName()}, new AppsMapper().getByName);
		if(result.size() > 0) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public List<Apps> getByType(Apps app) {
		
		String sql = "SELECT * FROM APPS WHERE TYPE_ID=?";
		List<Apps> result = jdbcTemplate.queryForObject(sql, new Object[] {app.getTypeId()}, new AppsMapper().getByType);
		
		return result;
		
	}

	@Override
	public Boolean insert(Apps app) {
		
		String sql = "INSERT INTO APPS(id, name, category_id, type_id) VALUES (?,?,?,?)";
		
		try {
			jdbcTemplate.update(sql, new Object[] {
				app.getId(),
				app.getName(),
				app.getCategoryId(),
				app.getTypeId()
			});
			
			return true;
		} catch (DataAccessException e) {
			e.printStackTrace();
			
			return false;
		}
		
	}
}
