/**
 * 
 */
package com.rest.dao;

import java.util.List;
import com.rest.bean.RequestDetail;

/**
 * @author Dimas Rifqi
 *
 */
public interface RequestDetailDao {
	public List<RequestDetail> getAll();
	public RequestDetail getById(RequestDetail rd);
	public RequestDetail getByJenkinsJobFinish(RequestDetail rd);
	public RequestDetail getByJenkinsJob(RequestDetail rd);
	public RequestDetail getByReqIdAndBuildNo(RequestDetail rd);
	public RequestDetail getByReqId(RequestDetail rd);
	public RequestDetail updateReqDetailByJenJobAndReqId(RequestDetail rd);
	public Boolean insert(RequestDetail rd);
}
