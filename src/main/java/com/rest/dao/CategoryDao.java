/**
 * 
 */
package com.rest.dao;

import java.util.List;

import com.rest.bean.Category;

/**
 * @author Dimas Rifqi
 *
 */
public interface CategoryDao {
	
	public List<Category> getAll();
	public Category getById(Category category);
	public Category getByName(Category category);
	public Boolean insert(Category category);

}
