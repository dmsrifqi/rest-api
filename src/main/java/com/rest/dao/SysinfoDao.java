/**
 * 
 */
package com.rest.dao;

import java.util.List;

import com.rest.bean.Sysinfo;

/**
 * @author Dimas Rifqi
 *
 */
public interface SysinfoDao {
	public List<Sysinfo> getAll();
	public Sysinfo getById(Sysinfo sys);
	public Boolean insert(Sysinfo sys);
}
