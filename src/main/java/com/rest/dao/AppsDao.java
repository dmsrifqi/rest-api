package com.rest.dao;

import java.util.List;
import com.rest.bean.Apps;

/**
 * @author Dimas Rifqi
 *
 */
public interface AppsDao {
	
	public List<Apps> getAll();
	public Apps getById(Apps app);
	public Apps getByName(Apps app);
	public List<Apps> getByType(Apps app);
	public Boolean insert(Apps app);
	
}
