/**
 * 
 */
package com.rest.dao;

import java.util.List;
import com.rest.bean.TypeRequest;

/**
 * @author Dimas Rifqi
 *
 */
public interface TypeRequestDao {
	public List<TypeRequest> getAll();
	public TypeRequest getById(TypeRequest type);
	public TypeRequest getByName(TypeRequest type);
	public Boolean insert(TypeRequest type);
}
