package com.rest.dao;

import java.util.List;
import com.rest.bean.JenkinsJob;

/**
 * @author Dimas Rifqi
 *
 */
public interface JenkinsJobDao {
	public List<JenkinsJob> getAll();
	public JenkinsJob getById(JenkinsJob job);
	public JenkinsJob getByName(JenkinsJob job);
	public Boolean insert(JenkinsJob job);
}
