/**
 * 
 */
package com.rest.dao;

import java.util.List;

import com.rest.bean.Type;

/**
 * @author Dimas Rifqi
 *
 */
public interface TypeDao {
	
	public List<Type> getAll();
	public Type getById(Type type);
	public Type getByName(Type type);
	public Boolean insert(Type type);

}
